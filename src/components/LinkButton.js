// import React from 'react';
// import './Button.css';
// import { Link } from 'react-router-dom';

// export function Button() {
//   return (
//     <Link to='sign-up'>
//       <button className='btn'>Sign Up</button>
//     </Link>
//   );
// }

import React from 'react';
import './Button.css';
import { Link } from 'react-router-dom';

const STYLES = ['btn--primary', "btn--secondary", "btn--success", 'btn--outline', 'btn--test'];

const SIZES = ['btn--medium', 'btn--large'];

export const LinkButton = ({
  children,
  type,
  onClick,
  buttonStyle,
  buttonSize,
  clickLink,
  btnWidth,
  isHeroLink
}) => {
  const checkButtonStyle = STYLES.includes(buttonStyle)
    ? buttonStyle
    : STYLES[0];

  const checkButtonSize = SIZES.includes(buttonSize) ? buttonSize : SIZES[0];

  return (
    <a href={clickLink} className='btn-mobile'>
      <button
        className={`btn ${checkButtonStyle} ${checkButtonSize} ${isHeroLink == "true" ? "hero-button" : ""}`}
        onClick={onClick}
        type={type}
        style={{width: {btnWidth}}}
      >
        {children}
      </button>
    </a>
  );
};
