import React, { useState, useRef } from 'react';

const VideoPlayer = (videoSrc) => {
    const [isPlaying, setIsPlaying] = useState(false);
    const videoRef = useRef(null);

    const togglePlay =() => {
        if (isPlaying) {
            videoRef.current.pause();
        } else {
            videoRef.current.play();
        }
        setIsPlaying(!isPlaying);
    };

    return (
        <div>
            <video ref={videoRef}
            controls>
                <source src={videoSrc}></source>
            </video>

            <button onClick={togglePlay}>
                {isPlaying ? "Pause" : "Play"}
            </button>
        </div>
    )
}

export default VideoPlayer;