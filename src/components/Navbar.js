import React, { useState, useEffect } from 'react';
import { Button } from './Button';
import { LinkButton } from './LinkButton';
import { Link } from 'react-router-dom';
import { FiExternalLink } from 'react-icons/fi'
import './Navbar.css';

function Navbar() {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton);

  return (
    <>
      <nav className='navbar'>
        <div className='navbar-container'>
          <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
            <img style={{ width: "260px" }} src='/images/logo.png' alt='navbar-logo'></img>
          </Link>
          <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
          </div>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                Home
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                to='/about'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                About
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                to='/contact'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Contact
              </Link>
            </li>
            <li className='nav-item'>
              <a
                href='https://docs.playherojumper.com'
                className='nav-links nav-links-blue'
                onClick={closeMobileMenu}
              >
                Level Editor Docs

                <FiExternalLink />
              </a>
            </li>
          </ul>
          {button &&
            <LinkButton clickLink="https://store.steampowered.com/app/2153600/Hero_Jumper/" buttonStyle='btn--outline'
              btnWidth="1000px">
              Buy On Steam

              <FiExternalLink />
            </LinkButton>}
        </div>
      </nav>
    </>
  );
}

export default Navbar;